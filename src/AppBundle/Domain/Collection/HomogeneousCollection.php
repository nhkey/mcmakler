<?php

namespace AppBundle\Domain\Collection;

use Doctrine\Common\Collections\ArrayCollection;

abstract class HomogeneousCollection extends ArrayCollection
{
    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException when element not accepted
     */
    public function __construct(array $elements = array())
    {
        foreach ($elements as $element) {
            $this->throwExceptionWhenNotAccepted($element);
        }
        parent::__construct($elements);
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException when element not accepted
     */
    public function set($key, $value)
    {
        $this->throwExceptionWhenNotAccepted($value);
        parent::set($key, $value);
    }

    /**
     * {@inheritdoc}
     * @throws \InvalidArgumentException when element not accepted
     */
    public function add($value)
    {
        $this->throwExceptionWhenNotAccepted($value);
        return parent::add($value);
    }

    /**
     * Method should throw exception if element
     *
     * @param mixed $element
     */
    abstract protected function throwExceptionWhenNotAccepted($element);
}
