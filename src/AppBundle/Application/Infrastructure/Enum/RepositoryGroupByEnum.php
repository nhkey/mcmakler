<?php

namespace AppBundle\Application\Infrastructure\Enum;

final class RepositoryGroupByEnum
{
    const YEAR = 'year';

    const MONTH = 'month';

}
