<?php

namespace ApiBundle\Application;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader;

final class ApiBundleExtension extends Extension
{
    private $bundlePath;

    public function __construct($bundlePath)
    {
        $this->bundlePath = $bundlePath;
    }

    private function getDiListFiles()
    {
        return [
            'param_converter.yml',
            'service.yml',
            'controller.yml',
            'event_listener.yml',
            'transformer.yml'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
       $this->loadDi($container);
    }

    private function loadDi($container)
    {
        $diPath = $this->bundlePath . '/DI';

        $loader = new Loader\YamlFileLoader($container, new FileLocator($diPath));
        foreach ($this->getDiListFiles() as $file) {
            if (file_exists($diPath . '/' . $file)) {
                $loader->load($diPath . '/' . $file);
            }
        }
    }
}
