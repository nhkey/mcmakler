<?php

namespace ApiBundle\Application\ParamConverter;

use Symfony\Component\HttpFoundation\Request;

trait ApiParamConverter
{
    /**
     * @return bool|string
     */
    protected function getBooleanParameter(Request $request, string $parameterName, $defaultValue = null)
    {
        $parameterValue = $request->get($parameterName, $defaultValue);
        $booleanValue = $this->getStrictBooleanValue($parameterValue);

        return is_null($booleanValue) ? $parameterValue : $booleanValue;
    }

    /**
     * @return float|string
     */
    protected function getFloatParameter(Request $request, string $parameterName, $defaultValue = null)
    {
        $parameterValue = $request->get($parameterName, $defaultValue);
        $floatValue = filter_var($parameterValue, FILTER_VALIDATE_FLOAT, FILTER_NULL_ON_FAILURE);

        return is_null($floatValue) ? $parameterValue : $floatValue;
    }

    /**
     * @return int|string
     */
    protected function getIntegerParameter(Request $request, string $parameterName, $defaultValue = null)
    {
        $parameterValue = $request->get($parameterName, $defaultValue);
        $integerValue = filter_var($parameterValue, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

        return is_null($integerValue) ? $parameterValue : $integerValue;
    }

    /**
     * @return bool|null
     */
    private function getStrictBooleanValue(string $value)
    {
        $truthyValues = ['true', '1', 'yes', 'on'];
        $falsyValues = ['false', '0', 'no', 'off'];

        if (is_bool($value)) {
            return $value;
        }

        $value = @strtolower((string)$value);

        return in_array($value, $truthyValues) ? true : (in_array($value, $falsyValues) ? false : null);
    }
}
