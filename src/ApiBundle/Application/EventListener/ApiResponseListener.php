<?php

namespace ApiBundle\Application\EventListener;

use ApiBundle\Application\Transformer\TransformerManager;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

final class ApiResponseListener
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var TransformerManager
     */
    private $transformerManager;

    public function __construct(
        LoggerInterface $logger,
        TransformerManager $transformerManager
    ) {
        $this->logger = $logger;
        $this->transformerManager = $transformerManager;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        $exceptionType = join('', array_slice(explode('\\', get_class($exception)), -1));

        $responseData = [
            'exception_type' => $exceptionType,
            'message' => $exception->getMessage(),
            'stack' => $exception->getTraceAsString()
        ];

        $this->logger->error('Error exception', $responseData);

        $event->setResponse(new JsonResponse(
            $responseData,
            Response::HTTP_BAD_REQUEST
        ));
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $event->setResponse(
            $this->createResponseWithData(
                $event->getControllerResult()
            )
        );
    }

    /**
     * @param mixed $data
     *
     * @return JsonResponse
     */
    private function createResponseWithData($data)
    {
        return new JsonResponse([
            $this->transformerManager->transform($data)
        ]);
    }
}