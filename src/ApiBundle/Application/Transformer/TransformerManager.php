<?php

namespace ApiBundle\Application\Transformer;

final class TransformerManager
{
    private $transformers;

    public function addTransformer(DtoTransformer $transformer): void
    {
        $this->transformers[$transformer->getSupportedClass()] = $transformer;
    }

    public function getTransformer(string $class): ?DtoTransformer
    {
        return $this->transformers[$class] ?? null;
    }

    /**
     * @param mixed $model
     *
     * @return mixed
     */
    public function transform($model)
    {
        if (is_array($model) || $model instanceof \IteratorAggregate) {
            $newCollection = [];
            foreach ($model as $key => $item) {
                $newCollection[$key] = $this->transform($item);
            }

            $model = $newCollection;
        }

        if (!is_object($model)) {
            return $model;
        }

        $transformer = $this->getTransformer(get_class($model));

        return is_null($transformer) ? $model : $transformer->toDto($model);
    }
}
