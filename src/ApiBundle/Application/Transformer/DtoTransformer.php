<?php

namespace ApiBundle\Application\Transformer;

interface DtoTransformer
{
    public function getSupportedClass(): string;

    /**
     * @param mixed $model
     *
     * @return mixed
     */
    public function toDto($model);
}
