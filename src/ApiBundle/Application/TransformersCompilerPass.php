<?php
namespace ApiBundle\Application;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class TransformersCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('api.transformer_manager')) {
            return;
        }

        $transformerManager = $container->findDefinition('api.transformer_manager');
        foreach ($this->getTransformers($container) as $transformer) {
            $transformerManager->addMethodCall(
                'addTransformer',
                [$transformer]
            );
        }

    }

    private function getTransformers(ContainerBuilder $container): array
    {
        $transformers = [];
        $taggedServices = $container->findTaggedServiceIds('api.transformer');
        foreach ($taggedServices as $serviceId => $attributes) {
            $transformers[] = $container->findDefinition($serviceId);
        }

        return $transformers;
    }
}
