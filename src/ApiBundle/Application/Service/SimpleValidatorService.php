<?php

namespace ApiBundle\Application\Service;

use ApiBundle\Application\Interfaces\RequestValidatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class SimpleValidatorService implements RequestValidatorService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @return string[]
     */
    public function validateRequest(Request $request): array
    {
        $requestParams = $request->attributes->get('_converters');
        if (!is_array($requestParams)) {
            return [];
        }
        $errors = [];
        /* @var ParamConverter $requestParam */
        foreach ($requestParams as $requestParam) {
            $requestParamName = $requestParam->getName();
            $requestParamInstance = $request->attributes->get($requestParamName);
            $errors = array_merge($this->validateObject($requestParamInstance));
        }
        return $errors;
    }

    /**
     * @param mixed $object
     * @param array $pathTree
     *
     * @return string[]
     */
    private function validateObject($object, array $pathTree = []): array
    {
        if (is_scalar($object)) {
            return [];
        }
        if (null === $object) {
            throw new \LogicException(
                sprintf('ParamConverter instance not found in the request with the key %s',
                    $object)
            );
        }

        $errors = $this->validator->validate($object);
        if ($errors->count() === 0) {
            return [];
        }
        return $this->formatted($errors, $pathTree);
    }

    private function formatted(ConstraintViolationListInterface $errors, array $pathTree = []): array
    {
        $formattedErrors = [];
        foreach ($errors as $error) {
            /** @var $error ConstraintViolationInterface */
            $propertyPath = implode('.', $pathTree) . '.' . $error->getPropertyPath();
            $formattedErrors[] =  $propertyPath . ': ' . $error->getMessage();
        }
        return $formattedErrors;
    }
}
