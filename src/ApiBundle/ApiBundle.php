<?php

namespace ApiBundle;

use ApiBundle\Application\ApiBundleExtension;
use ApiBundle\Application\TransformersCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ApiBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TransformersCompilerPass());
    }

    public function getContainerExtension(): ApiBundleExtension
    {
        return new ApiBundleExtension(__DIR__);
    }
}
