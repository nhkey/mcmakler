<?php

namespace NeoBundle\Application\ParamConverter;

use ApiBundle\Application\ParamConverter\ApiParamConverter;
use NeoBundle\Application\Model\NeoRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

final class NeoRequestParamConverter implements ParamConverterInterface
{
    use ApiParamConverter;

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration): bool
    {
        $neoRequest = new NeoRequest(
            $this->getBooleanParameter($request,'hazardous', false)
        );

        $request->attributes->set($configuration->getName(), $neoRequest);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration): bool
    {
        return $configuration->getClass() === NeoRequest::class;
    }
}
