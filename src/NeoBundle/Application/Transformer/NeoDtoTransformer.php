<?php

namespace NeoBundle\Application\Transformer;

use ApiBundle\Application\Transformer\DtoTransformer;
use NeoBundle\Domain\Model\Neo;

final class NeoDtoTransformer implements DtoTransformer
{
    public function getSupportedClass(): string
    {
        return Neo::class;
    }

    /**
     * {@inheritdoc}
     */
    public function toDto($model)
    {
        /** @var Neo $model */
        return [
            'id' => $model->getReferenceId(),
            'name' => $model->getName(),
            'speed' => $model->getSpeed(),
            'date' => $model->getDate()->format('Y-m-d'),
        ];
    }
}
