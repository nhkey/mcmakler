<?php

namespace NeoBundle\Application\Mapper;

use NeoBundle\Domain\Model\Neo as NeoModel;
use NeoBundle\Infrastructure\Document\Neo;

final class NeoSimpleMapper implements NeoMapper
{
    public function convertModelToEntity(NeoModel $neoModel): Neo
    {
        $neo = new Neo();
        $neo->setId($neoModel->getId());
        $neo->setName($neoModel->getName());
        $neo->setIsHazardous($neoModel->isHazardous());
        $neo->setReferenceId($neoModel->getReferenceId());
        $neo->setSpeed($neoModel->getSpeed());
        $neo->setDate($neoModel->getDate());

        return $neo;
    }

    public function convertEntityToModel(Neo $neo): NeoModel
    {
        return new NeoModel(
            $neo->getReferenceId(),
            $neo->getName(),
            $neo->getSpeed(),
            $neo->isHazardous(),
            $neo->getDate(),
            $neo->getId()
        );
    }
}
