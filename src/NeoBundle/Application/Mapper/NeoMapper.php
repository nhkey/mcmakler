<?php

namespace NeoBundle\Application\Mapper;

use NeoBundle\Domain\Model\Neo as NeoModel;
use NeoBundle\Infrastructure\Document\Neo;

interface NeoMapper
{
    public function convertModelToEntity(NeoModel $neo): Neo;

    public function convertEntityToModel(Neo $neo): NeoModel;
}
