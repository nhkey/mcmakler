<?php

namespace NeoBundle\Application\Model;

final class NeoRequest
{
    /**
     * @var bool
     *
     * @Assert\NotNull()
     * @Assert\Type(
     *     type="bool",
     *     message="The value {{ value }} is not a valid {{ type }}."
     * )
     */
    private $hazardous;

    /**
     * @param bool $hazardous
     */
    public function __construct($hazardous)
    {
        $this->hazardous = $hazardous;
    }

    public function getHazardous(): bool
    {
        return $this->hazardous;
    }
}
