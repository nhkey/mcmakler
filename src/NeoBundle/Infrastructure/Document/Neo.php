<?php

namespace NeoBundle\Infrastructure\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Bundle\MongoDBBundle\Validator\Constraints\Unique as MongoDBUnique;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document
 */
class Neo
{
    /**
     * @var string
     *
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var int
     *
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank()
     * @MongoDBUnique(fields="referenceId")
     */
    private $referenceId;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var float
     *
     * @MongoDB\Field(type="float")
     * @Assert\NotBlank()
     *
     */
    private $speed;

    /**
     * @var bool
     *
     * @MongoDB\Field(type="bool")
     * @Assert\NotBlank()
     */
    private $isHazardous;

    /**
     * @var \DateTime
     *
     * @MongoDB\Field(type="date")
     * @Assert\NotBlank()
     */
    private $date;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(?string $id)
    {
        $this->id = $id;
    }

    public function getReferenceId(): int
    {
        return $this->referenceId;
    }

    public function setReferenceId(int $referenceId)
    {
        $this->referenceId = $referenceId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getSpeed(): float
    {
        return $this->speed;
    }

    public function setSpeed(float $speed)
    {
        $this->speed = $speed;
    }

    public function isHazardous(): bool
    {
        return $this->isHazardous;
    }

    public function setIsHazardous(bool $isHazardous)
    {
        $this->isHazardous = $isHazardous;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    /**
     * Get isHazardous
     *
     * @return bool $isHazardous
     */
    public function getIsHazardous()
    {
        return $this->isHazardous;
    }
}
