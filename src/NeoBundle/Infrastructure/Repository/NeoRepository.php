<?php

namespace NeoBundle\Infrastructure\Repository;

use NeoBundle\Infrastructure\Document\Neo;

interface NeoRepository
{
    public function save(Neo $neo): Neo;

    public function findAll(array $conditions = [], array $orders = []): array;
}
