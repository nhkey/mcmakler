<?php

namespace NeoBundle\Infrastructure\Repository;

use Doctrine\ODM\MongoDB\DocumentManager;
use NeoBundle\Infrastructure\Document\Neo;

final class DoctrineRepository implements NeoRepository
{
    /**
     * @var DocumentManager
     */
    private $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

    public function save(Neo $neo): Neo
    {
        $this->documentManager->persist($neo);
        $this->documentManager->flush();

        return $neo;
    }

    /**
     * @param array $conditions
     * @param array $orders
     *
     * @return Neo[]
     */
    public function findAll(array $conditions = [], array $orders = []): array
    {
        $query =  $this->documentManager->createQueryBuilder('NeoBundle:Neo');

        if (count($conditions) > 0) {
            foreach ($conditions as $fieldName => $fieldValue) {
                $query->field($fieldName)->equals($fieldValue);
            }
        }

        if (count($orders) > 0) {
            foreach ($orders as $orderField => $order) {
                $query->sort($orderField, mb_strtolower($order));
            }
        }

        return $this->documentManager
            ->createQueryBuilder('NeoBundle:Neo')
            ->getQuery()
            ->execute()
            ->toArray();
    }
}
