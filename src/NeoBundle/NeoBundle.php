<?php

namespace NeoBundle;

use AppBundle\Application\AppBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class NeoBundle extends Bundle
{
    public function getContainerExtension(): AppBundleExtension
    {
        return new AppBundleExtension(__DIR__);
    }
}
