<?php

namespace NeoBundle\Presentation\Command;

use NeoBundle\Domain\Service\NeoProvider;
use NeoBundle\Domain\Service\NeoService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseCommand extends ContainerAwareCommand
{
    /**
     * @var NeoProvider
     */
    private $neoProvider;

    /**
     * @var NeoService
     */
    private $neoService;

    public function __construct (
        NeoProvider $neoProvider,
        NeoService $neoService
    ) {
        parent::__construct();

        $this->neoProvider = $neoProvider;
        $this->neoService = $neoService;
    }

    protected function configure()
    {
        $this
            ->setName('neo:parse')
            ->setDescription('Provide NEO from NASA');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $neoData = $this->neoProvider->getAllNeo();
        $this->neoService->createNeoFromCollection($neoData);
        $output->writeln('Parsing was completed. Count:' . $neoData->count());

    }
}