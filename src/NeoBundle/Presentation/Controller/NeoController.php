<?php

namespace NeoBundle\Presentation\Controller;

use NeoBundle\Application\Model\NeoRequest;
use NeoBundle\Domain\Service\NeoService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * @Route(service="neo_bundle.controller.neo")
 */
class NeoController extends Controller
{
    /**
     * @var NeoService
     */
    private $neoService;

    public function __construct(
        NeoService $neoService
    ) {
        $this->neoService = $neoService;
    }

    /**
     * @Method({"GET"})
     * @Route("neo/hazardous",
     *  name="_neo_get_all"
     * )
     */
    public function getAll()
    {
        $neoCollection = $this->neoService->getAll(true);

        return $neoCollection;
    }

    /**
     * @Method({"GET"})
     * @Route("neo/best-year",
     *  name="_neo_get_best_year"
     * )
     * @ParamConverter("neoRequest", converter="neo_request_converter")
     */
    public function getBestYear(NeoRequest $neoRequest)
    {
        $neoInfo = $this->neoService->getBestYearCollection($neoRequest->getHazardous());
        if (count($neoInfo) == 0) {
            return ['No data'];
        }
        return [
            'year' => $neoInfo['year'],
            'count' => $neoInfo['collection']->count(),
            'collection' => $neoInfo['collection']
        ];
    }

    /**
     * @Method({"GET"})
     * @Route("neo/best-month",
     *  name="_neo_get_best_month"
     * )
     * @ParamConverter("neoRequest", converter="neo_request_converter")
     */
    public function getBestMonth(NeoRequest $neoRequest)
    {
        $neoInfo = $this->neoService->getBestMonthCollection($neoRequest->getHazardous());
        if (count($neoInfo) == 0) {
            return ['No data'];
        }
        return [
            'month' => $neoInfo['month'],
            'count' => $neoInfo['collection']->count(),
            'collection' => $neoInfo['collection']
        ];
    }

    /**
     * @Method({"GET"})
     * @Route("neo/fastest",
     *  name="_neo_get_fastest"
     * )
     * @ParamConverter("neoRequest", converter="neo_request_converter")
     */
    public function getFastestNeo(NeoRequest $neoRequest)
    {
        $neoCollection = $this->neoService->getAll($neoRequest->getHazardous(), 'speed', 'DESC');

        return $neoCollection->first();
    }
}
