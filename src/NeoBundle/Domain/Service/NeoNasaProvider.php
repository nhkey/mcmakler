<?php

namespace NeoBundle\Domain\Service;

use GuzzleHttp\Client;
use NeoBundle\Domain\Collection\NeoCollection;
use NeoBundle\Domain\Model\Neo;

final class NeoNasaProvider implements NeoProvider
{
    const DAYS = 3;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $apiKey;

    public function __construct(
        Client $client,
        string $url,
        string $apiKey
    ) {
        $this->client = $client;
        $this->url = $url;
        $this->apiKey = $apiKey;
    }

    private function parseNasa()
    {
        $endDate = new \DateTime();
        $startDate = clone $endDate;
        $startDate->modify("-" . self::DAYS . " days");
        $params = http_build_query([
            'api_key' => $this->apiKey,
            'start_date' => $startDate->format('Y-m-d'),
            'end_date' => $endDate->format('Y-m-d'),
        ]);

        $res = $this->client->request('GET', $this->url . "/?" . $params);

        return \GuzzleHttp\json_decode($res->getBody(), 1);
    }

    public function getAllNeo(): NeoCollection
    {
        $neoData = $this->parseNasa();
        $neoCollection = new NeoCollection();
        $neos = $neoData['near_earth_objects'] ?? [];
        foreach ($neos as $neoInDay) {
            foreach ($neoInDay as $neo) {
                $neoModel = new Neo(
                    $neo['neo_reference_id'],
                    $neo['name'],
                    $neo['close_approach_data'][0]['relative_velocity']['kilometers_per_hour'],
                    $neo['is_potentially_hazardous_asteroid'],
                    new \DateTime($neo['close_approach_data'][0]['close_approach_date'])
                );
                $neoCollection->add($neoModel);
            }
        }
        return $neoCollection;
    }
}
