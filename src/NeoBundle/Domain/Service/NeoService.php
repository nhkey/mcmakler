<?php

namespace NeoBundle\Domain\Service;

use AppBundle\Application\Infrastructure\Enum\RepositoryGroupByEnum;
use NeoBundle\Application\Mapper\NeoMapper;
use NeoBundle\Domain\Collection\NeoCollection;
use NeoBundle\Domain\Model\Neo as NeoModel;
use NeoBundle\Infrastructure\Document\Neo;
use NeoBundle\Infrastructure\Repository\NeoRepository;

final class NeoService
{
    /**
     * @var NeoRepository
     */
    private $neoRepository;

    /**
     * @var NeoMapper
     */
    private $neoMapper;

    public function __construct(
        NeoRepository $neoRepository,
        NeoMapper $neoMapper
    ) {
        $this->neoRepository = $neoRepository;
        $this->neoMapper = $neoMapper;
    }

    public function createNeo(NeoModel $neo) : bool
    {
        $neoEntity = $this->neoMapper->convertModelToEntity($neo);
        $this->neoRepository->save($neoEntity);

        return true;
    }

    public function createNeoFromCollection(NeoCollection $neoCollection): bool
    {
        $neoCollection->forAll(function($_, $neo) {
            $this->createNeo($neo);
        });

        return true;
    }

    public function getAll(bool $isHazardous, string $orderBy = null, string $order = 'DESC'): NeoCollection
    {
        $orders = $orderBy ? [$orderBy => $order] : [];

        $neoEntityArray = $this->neoRepository->findAll(
            ['hazardous' => $isHazardous],
            $orders
        );
        $neoCollection = new NeoCollection();
        foreach ($neoEntityArray as $neoEntity) {
            /** @var Neo $neoEntity */
            $neoCollection->add(
                $this->neoMapper->convertEntityToModel($neoEntity)
            );
        }

        return $neoCollection;
    }

    public function getBestYearCollection($isHazardous): array
    {
        $neoEntityArray = $this->neoRepository->findAll(['hazardous' => $isHazardous]);
        if (count($neoEntityArray) == 0) {
            return [];
        }

        $neoCollection = new NeoCollection(
            array_map(
                function(Neo $neoEntity) {
                    return $this->neoMapper->convertEntityToModel($neoEntity);
                },
                $neoEntityArray
            )
        );
        $neoCollections = $this->separateCollectionByYear($neoCollection);

        return [
            'year' => key($neoCollections),
            'collection' => reset($neoCollections),
        ];
    }

    public function getBestMonthCollection($isHazardous): array
    {
        $neoEntityArray = $this->neoRepository->findAll(['hazardous' => $isHazardous]);
        if (count($neoEntityArray) == 0) {
            return [];
        }

        $neoCollection = new NeoCollection(
            array_map(
                function(Neo $neoEntity) {
                    return $this->neoMapper->convertEntityToModel($neoEntity);
                },
                $neoEntityArray
            )
        );
        $neoCollections = $this->separateCollectionByMonth($neoCollection);

        return [
            'month' => key($neoCollections),
            'collection' => reset($neoCollections),
        ];
    }

    /**
     * @param NeoCollection $neoCollection
     *
     * @return NeoCollection[]
     */
    private function separateCollectionByYear(NeoCollection $neoCollection): array
    {
        $neoCollections = [];
        foreach ($neoCollection as $neo) {
            /** @var Neo $neo */
            $year = $neo->getDate()->format('Y');
            if (!isset($neoCollections[$year])) {
                $neoCollections[$year] = new NeoCollection();
            }
            $neoCollections[$year]->add($neo);
        }

        uasort($neoCollections, function(NeoCollection $a, NeoCollection $b) {
            return $b->count() - $a->count();
        });

        return $neoCollections;
    }


    /**
     * @param NeoCollection $neoCollection
     *
     * @return NeoCollection[]
     */
    private function separateCollectionByMonth(NeoCollection $neoCollection): array
    {
        $neoCollections = [];
        foreach ($neoCollection as $neo) {
            /** @var Neo $neo */
            $month = $neo->getDate()->format('m');
            if (!isset($neoCollections[$month])) {
                $neoCollections[$month] = new NeoCollection();
            }
            $neoCollections[$month]->add($neo);
        }

        uasort($neoCollections, function(NeoCollection $a, NeoCollection $b) {
            return $b->count() - $a->count();
        });

        return $neoCollections;
    }
}
