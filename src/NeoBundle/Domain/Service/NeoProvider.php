<?php

namespace NeoBundle\Domain\Service;

use NeoBundle\Domain\Collection\NeoCollection;

interface NeoProvider
{
    public function getAllNeo(): NeoCollection;
}
