<?php

namespace NeoBundle\Domain\Model;

final class Neo
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var int
     */
    private $referenceId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $speed;

    /**
     * @var bool
     */
    private $isHazardous;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @param int $referenceId
     * @param string $name
     * @param float $speed
     * @param bool $isHazardous
     * @param \DateTime $date
     * @param int|null $id
     */
    public function __construct(
        int $referenceId,
        string $name,
        float $speed,
        bool $isHazardous,
        \DateTime $date,
        $id = null
    ) {
        $this->referenceId = $referenceId;
        $this->name = $name;
        $this->speed = $speed;
        $this->isHazardous = $isHazardous;
        $this->date = $date;
        $this->id = $id;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getReferenceId(): int
    {
        return $this->referenceId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSpeed(): float
    {
        return $this->speed;
    }

    public function isHazardous(): bool
    {
        return $this->isHazardous;
    }

    public function getDate(): \DateTime
    {
        return $this->date;
    }
}
