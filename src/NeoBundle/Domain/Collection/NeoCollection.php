<?php

namespace NeoBundle\Domain\Collection;

use AppBundle\Domain\Collection\HomogeneousCollection;
use NeoBundle\Domain\Model\Neo;

final class NeoCollection extends HomogeneousCollection
{
    protected function throwExceptionWhenNotAccepted($element)
    {
        if (!is_object($element) || !$element instanceof Neo) {
            throw new \InvalidArgumentException(
                "The given element is not accepted by the collection: "
                . var_export($element, true)
            );
        }
    }
}
