Feature: Get Hazardous NEO
  Background:
    Given There are neos:
      | name      | speed | is_hazardous | date       | reference_id |
      | Name1     | 100   | true         | 2017-01-01 | 1            |
      | Name2     | 200   | false        | 2016-02-03 | 2            |
      | Name3     | 250   | true         | 2015-01-03 | 3            |
      | Name4     | 300   | false        | 2016-01-02 | 4            |

    Scenario:
      When I open "/neo/hazardous"
      Then I see valid response
      And response should fit with "hazardous.json"

