Feature: Get Hazardous NEO
  Background:
    Given There are neos:
      | name      | speed | is_hazardous | date       | reference_id |
      | Name1     | 100   | true         | 2017-01-01 | 1            |
      | Name2     | 200   | false        | 2016-02-03 | 2            |
      | Name3     | 250   | true         | 2016-01-03 | 3            |
      | Name4     | 300   | false        | 2016-01-02 | 4            |
      | Name5     | 300   | true         | 2014-02-02 | 5            |
      | Name6     | 300   | true         | 2014-02-12 | 6            |
      | Name7     | 300   | false        | 2012-03-17 | 7            |
      | Name8     | 300   | false        | 2012-01-17 | 8            |
      | Name9     | 300   | false        | 2012-02-27 | 9            |

  Scenario:
    When I open "/neo/best-month?hazardous=true"
    Then I see valid response
    And response should fit with "month_hazardous.json"
  @test
  Scenario:
    When I open "/neo/best-month?hazardous=false"
    Then I see valid response
    And response should fit with "month_not_hazardous.json"

  Scenario:
    When I open "/neo/best-month"
    Then I see valid response
    And response should fit with "month_not_hazardous.json"
