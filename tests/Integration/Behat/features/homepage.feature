Feature: Homepage endpoint

  Scenario: Open homepage
    When I open "/"
    Then I see valid response
    And response should fit with "homepage.json"
