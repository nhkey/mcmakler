<?php

namespace Test\Behat\TestRepository;

use NeoBundle\Infrastructure\Document\Neo;
use NeoBundle\Infrastructure\Repository\NeoRepository;

final class TestNeoRepository implements NeoRepository
{
    /**
     * @var Neo[]
     */
    private $neos;

    /**
     * @var Neo[]
     */
    private $hazardous;

    /**
     * @var Neo[]
     */
    private $notHazardous;

    public function save(Neo $neo): Neo
    {
        $neo->setId($neo->getReferenceId());
        $this->neos[] = $neo;
        $neo->isHazardous() ? $this->hazardous[] = $neo : $this->notHazardous[] = $neo;

        return $neo;
    }

    public function findAll(array $conditions = [], array $orders = []): array
    {
        if (!isset($conditions['hazardous'])) {
            return $this->neos;
        }

        if (isset($orders['speed'])) {

            usort($this->notHazardous, function(Neo $a, Neo $b) {
                return $b->getSpeed() - $a->getSpeed();
            });
            usort($this->hazardous, function(Neo $a, Neo $b) {
                return $b->getSpeed() - $a->getSpeed();
            });
        }

        return $conditions['hazardous'] ? $this->hazardous : $this->notHazardous;
    }
}
