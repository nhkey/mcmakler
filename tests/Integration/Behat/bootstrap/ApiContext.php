<?php

namespace Test\Behat;

use Behat\Behat\Context\Context;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use PHPUnit\Framework\Assert;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Component\HttpFoundation\Response;

class ApiContext implements Context, KernelAwareContext
{
    use KernelDictionary;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array
     */
    protected $clientResponse;

    /**
     * @var int
     */
    protected $clientHttpStatus;

    protected function doRequest($url, $method, $body = [], $contentType = 'application/json')
    {
        $headers = [];
        if ($contentType && $method !== 'GET') {
            $headers['CONTENT_TYPE'] = $contentType;
        }

        $this->getClient()->request(
            strtolower($method),
            $url,
            [],
            [],
            $headers,
            $body
        );
        $response = $this->getClient()->getResponse();

        $this->clientResponse = $response->getContent();
        $this->clientHttpStatus = $response->getStatusCode();
    }

    /**
     * @When I open :url
     */
    public function iOpen($url)
    {
        $this->doRequest($url, 'GET');
    }

    /**
     * @Then I see valid response
     */
    public function iSeeValidResponse()
    {
        Assert::assertEquals($this->clientHttpStatus, Response::HTTP_OK);
    }

    /**
     * @Then response should fit with :json
     */
    public function responseShouldFitWith($json)
    {
        $this->assertResponseMatchesDump($json, $this->clientResponse);
    }


    protected function getClient(): Client
    {
        if (null === $this->client) {
            $this->client = $this->getContainer()->get('test.client');
        }

        return $this->client;
    }

    protected function assertResponseMatchesDump(string $fileName, string $removeVariableData)
    {
        Assert::assertEquals(
            json_decode($removeVariableData, true),
            $this->getArrayFromJsonFile($fileName)
        );
    }

    protected function getArrayFromJsonFile(string $filename): array
    {
        $content = file_get_contents(__DIR__ . '/../json/' . $filename);

        return json_decode($content, true);
    }
}
