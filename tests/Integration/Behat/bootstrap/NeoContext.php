<?php

namespace Test\Behat;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Behat\Symfony2Extension\Context\KernelDictionary;
use NeoBundle\Infrastructure\Document\Neo;
use Test\Behat\TestRepository\TestNeoRepository;

class NeoContext implements Context, KernelAwareContext
{
    use KernelDictionary;

    private $repositoryIsMocked = false;

    /**
     * @var TestNeoRepository
     */
    private $repository;

    /**
     * @Given There are neos:
     */
    public function createNeoFromTable(TableNode $table)
    {
        $tableHash = $table->getHash();
        foreach ($tableHash as $row) {
            $neo = new Neo();
            $neo->setSpeed($row['speed']);
            $neo->setReferenceId($row['reference_id']);
            $neo->setIsHazardous($row['is_hazardous'] === 'true');
            $neo->setName($row['name']);
            $neo->setDate(\DateTime::createFromFormat('Y-m-d', $row['date']));

            $this->saveNeo($neo);
        }
    }

    public function saveNeo(Neo $neo)
    {
        if (!$this->repositoryIsMocked) {
            $this->mockRepository();
        }
        $this->repository->save($neo);
    }

    private function mockRepository()
    {
        $this->repository = new TestNeoRepository();
        $this->getContainer()->set('neo_bundle.repository.neo', $this->repository);
        $this->repositoryIsMocked = true;
    }
}
