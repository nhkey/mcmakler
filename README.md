Hello.

It's my solution of your test. Yes, I can do the test more simple, but I should show to you my best skills.
I didn't complete some things:
 - Unit-tests
 - Script for add hosts and run composer install.
 - Clean up Symfony's configs
 - Separate DI-config (service.yml)
 - Add some interfaces (e.g. for NeoService)
 - Refactoring

Probably I forgot something else.

I spent ~ 9 hours 30 minutes for the test.

Install
---------

You need Docker for running the application.

```
docker-compose build && docker-compose 
```

Unfortunately, I didn't write script and you should run ```composer install```.
 If you have error with mongo, you can try repeat with a flag: ```composer install --ignore-platform-reqs```
Also you should add ```127.0.0.1   mcmaker.local``` to hosts-file.


Work
-------
Available endpoints:
 - /
 - /neo/hazardous
 - /neo/fastest?hazardous=(true|false)
 - /neo/best-month?hazardous=(true|false)
 - /neo/best-year?hazardous=(true|false)

Also you can run command ```./bin/console neo:parse``` for parsing last 3 days.
For running test ```./bin/behat```

Please let me know in case you have any questions.
